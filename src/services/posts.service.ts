import axios from "axios";
import { PostDto } from "models/post.dto";
import { POSTS_URL } from "./url";

export const getPosts = async () => {
  let result: PostDto[] = [];
  await axios.get(POSTS_URL).then((res) => (result = res.data));
  return result;
};

export const getOne = async (id: number) => await axios.get(`${POSTS_URL}/${id}/`).then();

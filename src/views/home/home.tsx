/* eslint-disable jsx-a11y/iframe-has-title */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { FC } from "react";

export const Home: FC = () => (
  <>
    <section id="hero">
      <div className="hero-container">
        <div id="heroCarousel" className="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="5000">
          <ol id="hero-carousel-indicators" className="carousel-indicators"></ol>

          <div className="carousel-inner" role="listbox">
            <div className="carousel-item active" style={{ backgroundImage: "url(assets/img/hero-carousel/1.jpg)" }}>
              <div className="carousel-container">
                <div className="container">
                  <h2 className="animate__animated animate__fadeInDown">The Best Business Information </h2>
                  <p className="animate__animated animate__fadeInUp">
                    We're In The Business Of Helping You Start Your Business
                  </p>
                  <a href="#about" className="btn-get-started scrollto animate__animated animate__fadeInUp">
                    Get Started
                  </a>
                </div>
              </div>
            </div>

            <div className="carousel-item" style={{ backgroundImage: "url(assets/img/hero-carousel/2.jpg)" }}>
              <div className="carousel-container">
                <div className="container">
                  <h2 className="animate__animated animate__fadeInDown">At vero eos et accusamus</h2>
                  <p className="animate__animated animate__fadeInUp">
                    Helping Business Security & Peace of Mind for Your Family
                  </p>
                  <a href="#about" className="btn-get-started scrollto animate__animated animate__fadeInUp">
                    Get Started
                  </a>
                </div>
              </div>
            </div>

            <div className="carousel-item" style={{ backgroundImage: "url(assets/img/hero-carousel/3.jpg)" }}>
              <div className="carousel-container">
                <div className="container">
                  <h2 className="animate__animated animate__fadeInDown">Temporibus autem quibusdam</h2>
                  <p className="animate__animated animate__fadeInUp">
                    Beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                  </p>
                  <a href="#about" className="btn-get-started scrollto animate__animated animate__fadeInUp">
                    Get Started
                  </a>
                </div>
              </div>
            </div>
          </div>

          <a className="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
            <span className="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
          </a>

          <a className="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
            <span className="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
          </a>
        </div>
      </div>
    </section>
    {/* End Hero Section */}

    <main id="main">
      {/* ======= About Section ======= */}
      <div id="about" className="about-area area-padding">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12">
              <div className="section-headline text-center">
                <h2>About eBusiness</h2>
              </div>
            </div>
          </div>
          <div className="row">
            {/* single-well start*/}
            <div className="col-md-6 col-sm-6 col-xs-12">
              <div className="well-left">
                <div className="single-well">
                  <a href="#">
                    <img src="assets/img/about/1.jpg" alt="" />
                  </a>
                </div>
              </div>
            </div>
            {/* single-well end*/}
            <div className="col-md-6 col-sm-6 col-xs-12">
              <div className="well-middle">
                <div className="single-well">
                  <a href="#">
                    <h4 className="sec-head">project Maintenance</h4>
                  </a>
                  <p>
                    Redug Lagre dolor sit amet, consectetur adipisicing elit. Itaque quas officiis iure aspernatur sit
                    adipisci quaerat unde at nequeRedug Lagre dolor sit amet, consectetur adipisicing elit. Itaque quas
                    officiis iure
                  </p>
                  <ul>
                    <li>
                      <i className="bi bi-check"></i> Interior design Package
                    </li>
                    <li>
                      <i className="bi bi-check"></i> Building House
                    </li>
                    <li>
                      <i className="bi bi-check"></i> Reparing of Residentail Roof
                    </li>
                    <li>
                      <i className="bi bi-check"></i> Renovaion of Commercial Office
                    </li>
                    <li>
                      <i className="bi bi-check"></i> Make Quality Products
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            {/* End col*/}
          </div>
        </div>
      </div>
      {/* End About Section */}

      {/* ======= Services Section ======= */}
      <div id="services" className="services-area area-padding">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12">
              <div className="section-headline services-head text-center">
                <h2>Our Services</h2>
              </div>
            </div>
          </div>
          <div className="row text-center">
            {/* Start Left services */}
            <div className="col-md-4 col-sm-4 col-xs-12">
              <div className="about-move">
                <div className="services-details">
                  <div className="single-services">
                    <a className="services-icon" href="#">
                      <i className="bi bi-briefcase"></i>
                    </a>
                    <h4>Expert Coder</h4>
                    <p>
                      will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype
                      looks finished by.
                    </p>
                  </div>
                </div>
                {/* end about-details */}
              </div>
            </div>
            <div className="col-md-4 col-sm-4 col-xs-12">
              <div className="about-move">
                <div className="services-details">
                  <div className="single-services">
                    <a className="services-icon" href="#">
                      <i className="bi bi-card-checklist"></i>
                    </a>
                    <h4>Creative Designer</h4>
                    <p>
                      will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype
                      looks finished by.
                    </p>
                  </div>
                </div>
                {/* end about-details */}
              </div>
            </div>
            <div className="col-md-4 col-sm-4 col-xs-12">
              {/* end col-md-4 */}
              <div className=" about-move">
                <div className="services-details">
                  <div className="single-services">
                    <a className="services-icon" href="#">
                      <i className="bi bi-bar-chart"></i>
                    </a>
                    <h4>Wordpress Developer</h4>
                    <p>
                      will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype
                      looks finished by.
                    </p>
                  </div>
                </div>
                {/* end about-details */}
              </div>
            </div>
            <div className="col-md-4 col-sm-4 col-xs-12">
              {/* end col-md-4 */}
              <div className=" about-move">
                <div className="services-details">
                  <div className="single-services">
                    <a className="services-icon" href="#">
                      <i className="bi bi-binoculars"></i>
                    </a>
                    <h4>Social Marketer </h4>
                    <p>
                      will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype
                      looks finished by.
                    </p>
                  </div>
                </div>
                {/* end about-details */}
              </div>
            </div>
            {/* End Left services */}
            <div className="col-md-4 col-sm-4 col-xs-12">
              {/* end col-md-4 */}
              <div className=" about-move">
                <div className="services-details">
                  <div className="single-services">
                    <a className="services-icon" href="#">
                      <i className="bi bi-brightness-high"></i>
                    </a>
                    <h4>Seo Expart</h4>
                    <p>
                      will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype
                      looks finished by.
                    </p>
                  </div>
                </div>
                {/* end about-details */}
              </div>
            </div>
            {/* End Left services */}
            <div className="col-md-4 col-sm-4 col-xs-12">
              {/* end col-md-4 */}
              <div className=" about-move">
                <div className="services-details">
                  <div className="single-services">
                    <a className="services-icon" href="#">
                      <i className="bi bi-calendar4-week"></i>
                    </a>
                    <h4>24/7 Support</h4>
                    <p>
                      will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype
                      looks finished by.
                    </p>
                  </div>
                </div>
                {/* end about-details */}
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* End Services Section */}

      {/* ======= Team Section ======= */}
      <div id="team" className="our-team-area area-padding">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12">
              <div className="section-headline text-center">
                <h2>Our special Team</h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-3 col-sm-3 col-xs-12">
              <div className="single-team-member">
                <div className="team-img">
                  <a href="#">
                    <img src="assets/img/team/1.jpg" alt="" />
                  </a>
                  <div className="team-social-icon text-center">
                    <ul>
                      <li>
                        <a href="#">
                          <i className="bi bi-facebook"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="bi bi-twitter"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="bi bi-instagram"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="team-content text-center">
                  <h4>Jhon Mickel</h4>
                  <p>Seo</p>
                </div>
              </div>
            </div>
            {/* End column */}
            <div className="col-md-3 col-sm-3 col-xs-12">
              <div className="single-team-member">
                <div className="team-img">
                  <a href="#">
                    <img src="assets/img/team/2.jpg" alt="" />
                  </a>
                  <div className="team-social-icon text-center">
                    <ul>
                      <li>
                        <a href="#">
                          <i className="bi bi-facebook"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="bi bi-twitter"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="bi bi-instagram"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="team-content text-center">
                  <h4>Andrew Arnold</h4>
                  <p>Web Developer</p>
                </div>
              </div>
            </div>
            {/* End column */}
            <div className="col-md-3 col-sm-3 col-xs-12">
              <div className="single-team-member">
                <div className="team-img">
                  <a href="#">
                    <img src="assets/img/team/3.jpg" alt="" />
                  </a>
                  <div className="team-social-icon text-center">
                    <ul>
                      <li>
                        <a href="#">
                          <i className="bi bi-facebook"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="bi bi-twitter"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="bi bi-instagram"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="team-content text-center">
                  <h4>Lellien Linda</h4>
                  <p>Web Design</p>
                </div>
              </div>
            </div>
            {/* End column */}
            <div className="col-md-3 col-sm-3 col-xs-12">
              <div className="single-team-member">
                <div className="team-img">
                  <a href="#">
                    <img src="assets/img/team/4.jpg" alt="" />
                  </a>
                  <div className="team-social-icon text-center">
                    <ul>
                      <li>
                        <a href="#">
                          <i className="bi bi-facebook"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="bi bi-twitter"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="bi bi-instagram"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="team-content text-center">
                  <h4>Jhon Powel</h4>
                  <p>Seo Expert</p>
                </div>
              </div>
            </div>
            {/* End column */}
          </div>
        </div>
      </div>
      {/* End Team Section */}

      {/* ======= Rviews Section ======= */}
      <div className="reviews-area">
        <div className="row g-0">
          <div className="col-lg-6">
            <img src="assets/img/about/2.jpg" alt="" className="img-fluid" />
          </div>
          <div className="col-lg-6 work-right-text d-flex align-items-center">
            <div className="px-5 py-5 py-lg-0">
              <h2>working with us</h2>
              <h5>Web Design, Ready Home, Construction and Co-operate Outstanding Buildings.</h5>
              <a href="#contact" className="ready-btn scrollto">
                Contact us
              </a>
            </div>
          </div>
        </div>
      </div>
      {/* End Rviews Section */}

      {/* ======= Portfolio Section ======= */}
      <div id="portfolio" className="portfolio-area area-padding fix">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div className="section-headline text-center">
                <h2>Our Portfolio</h2>
              </div>
            </div>
          </div>
          <div className="row wesome-project-1 fix">
            {/* Start Portfolio -page */}
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <ul id="portfolio-flters">
                <li data-filter="*" className="filter-active">
                  All
                </li>
                <li data-filter=".filter-app">App</li>
                <li data-filter=".filter-card">Card</li>
                <li data-filter=".filter-web">Web</li>
              </ul>
            </div>
          </div>

          <div className="row awesome-project-content portfolio-container">
            {/* portfolio-item start */}
            <div className="col-md-4 col-sm-4 col-xs-12 portfolio-item filter-app portfolio-item">
              <div className="single-awesome-project">
                <div className="awesome-img">
                  <a href="#">
                    <img src="assets/img/portfolio/1.jpg" alt="" />
                  </a>
                  <div className="add-actions text-center">
                    <div className="project-dec">
                      <a className="portfolio-lightbox" data-gallery="myGallery" href="assets/img/portfolio/1.jpg">
                        <h4>Business City</h4>
                        <span>Web Development</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* portfolio-item end */}

            {/* portfolio-item start */}
            <div className="col-md-4 col-sm-4 col-xs-12 portfolio-item filter-web">
              <div className="single-awesome-project">
                <div className="awesome-img">
                  <a href="#">
                    <img src="assets/img/portfolio/2.jpg" alt="" />
                  </a>
                  <div className="add-actions text-center">
                    <div className="project-dec">
                      <a className="portfolio-lightbox" data-gallery="myGallery" href="assets/img/portfolio/2.jpg">
                        <h4>Blue Sea</h4>
                        <span>Photosho</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* portfolio-item end */}

            {/* portfolio-item start */}
            <div className="col-md-4 col-sm-4 col-xs-12 portfolio-item filter-card">
              <div className="single-awesome-project">
                <div className="awesome-img">
                  <a href="#">
                    <img src="assets/img/portfolio/3.jpg" alt="" />
                  </a>
                  <div className="add-actions text-center">
                    <div className="project-dec">
                      <a className="portfolio-lightbox" data-gallery="myGallery" href="assets/img/portfolio/3.jpg">
                        <h4>Beautiful Nature</h4>
                        <span>Web Design</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* portfolio-item end */}

            {/* portfolio-item start */}
            <div className="col-md-4 col-sm-4 col-xs-12 portfolio-item filter-web">
              <div className="single-awesome-project">
                <div className="awesome-img">
                  <a href="#">
                    <img src="assets/img/portfolio/4.jpg" alt="" />
                  </a>
                  <div className="add-actions text-center">
                    <div className="project-dec">
                      <a className="portfolio-lightbox" data-gallery="myGallery" href="assets/img/portfolio/4.jpg">
                        <h4>Creative Team</h4>
                        <span>Web design</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* portfolio-item end */}

            {/* portfolio-item start */}
            <div className="col-md-4 col-sm-4 col-xs-12 portfolio-item filter-app">
              <div className="single-awesome-project">
                <div className="awesome-img">
                  <a href="#">
                    <img src="assets/img/portfolio/5.jpg" alt="" />
                  </a>
                  <div className="add-actions text-center text-center">
                    <div className="project-dec">
                      <a className="portfolio-lightbox" data-gallery="myGallery" href="assets/img/portfolio/5.jpg">
                        <h4>Beautiful Flower</h4>
                        <span>Web Development</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* portfolio-item end */}

            {/* portfolio-item start */}
            <div className="col-md-4 col-sm-4 col-xs-12 portfolio-item filter-web">
              <div className="single-awesome-project">
                <div className="awesome-img">
                  <a href="#">
                    <img src="assets/img/portfolio/6.jpg" alt="" />
                  </a>
                  <div className="add-actions text-center">
                    <div className="project-dec">
                      <a className="portfolio-lightbox" data-gallery="myGallery" href="assets/img/portfolio/6.jpg">
                        <h4>Night Hill</h4>
                        <span>Photoshop</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* portfolio-item end */}
          </div>
        </div>
      </div>
      {/* End Portfolio Section */}

      {/* ======= Pricing Section ======= */}
      <div id="pricing" className="pricing-area area-padding">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12">
              <div className="section-headline text-center">
                <h2>Pricing Table</h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4 col-sm-4 col-xs-12">
              <div className="pri_table_list">
                <h3>
                  basic <br /> <span>$80 / month</span>
                </h3>
                <ol>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Online system</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-x"></i>
                    <span>Full access</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Free apps</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Multiple slider</span>
                  </li>
                  <li className="cross">
                    <i className="bi bi-x"></i>
                    <span>Free domin</span>
                  </li>
                  <li className="cross">
                    <i className="bi bi-x"></i>
                    <span>Support unlimited</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Payment online</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-x"></i>
                    <span>Cash back</span>
                  </li>
                </ol>
                <button>sign up now</button>
              </div>
            </div>
            <div className="col-md-4 col-sm-4 col-xs-12">
              <div className="pri_table_list active">
                <span className="saleon">top sale</span>
                <h3>
                  standard <br /> <span>$110 / month</span>
                </h3>
                <ol>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Online system</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Full access</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Free apps</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Multiple slider</span>
                  </li>
                  <li className="cross">
                    <i className="bi bi-x"></i>
                    <span>Free domin</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Support unlimited</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Payment online</span>
                  </li>
                  <li className="cross">
                    <i className="bi bi-x"></i>
                    <span>Cash back</span>
                  </li>
                </ol>
                <button>sign up now</button>
              </div>
            </div>
            <div className="col-md-4 col-sm-4 col-xs-12">
              <div className="pri_table_list">
                <h3>
                  premium <br /> <span>$150 / month</span>
                </h3>
                <ol>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Online system</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Full access</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Free apps</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Multiple slider</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Free domin</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Support unlimited</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Payment online</span>
                  </li>
                  <li className="check">
                    <i className="bi bi-check"></i>
                    <span>Cash back</span>
                  </li>
                </ol>
                <button>sign up now</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* End Pricing Section */}

      {/* ======= Testimonials Section ======= */}
      <div id="testimonials" className="testimonials">
        <div className="container">
          <div className="testimonials-slider swiper-container">
            <div className="swiper-wrapper">
              <div className="swiper-slide">
                <div className="testimonial-item">
                  <img src="assets/img/testimonials/testimonials-1.jpg" className="testimonial-img" alt="" />
                  <h3>Saul Goodman</h3>
                  <h4>Ceo &amp; Founder</h4>
                  <p>
                    <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                    Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium
                    quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                    <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                  </p>
                </div>
              </div>
              {/* End testimonial item */}

              <div className="swiper-slide">
                <div className="testimonial-item">
                  <img src="assets/img/testimonials/testimonials-2.jpg" className="testimonial-img" alt="" />
                  <h3>Sara Wilsson</h3>
                  <h4>Designer</h4>
                  <p>
                    <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                    Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis
                    quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                    <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                  </p>
                </div>
              </div>
              {/* End testimonial item */}

              <div className="swiper-slide">
                <div className="testimonial-item">
                  <img src="assets/img/testimonials/testimonials-3.jpg" className="testimonial-img" alt="" />
                  <h3>Jena Karlis</h3>
                  <h4>Store Owner</h4>
                  <p>
                    <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                    Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor
                    labore quem eram duis noster aute amet eram fore quis sint minim.
                    <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                  </p>
                </div>
              </div>
              {/* End testimonial item */}

              <div className="swiper-slide">
                <div className="testimonial-item">
                  <img src="assets/img/testimonials/testimonials-4.jpg" className="testimonial-img" alt="" />
                  <h3>Matt Brandon</h3>
                  <h4>Freelancer</h4>
                  <p>
                    <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                    Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim
                    dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                    <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                  </p>
                </div>
              </div>
              {/* End testimonial item */}

              <div className="swiper-slide">
                <div className="testimonial-item">
                  <img src="assets/img/testimonials/testimonials-5.jpg" className="testimonial-img" alt="" />
                  <h3>John Larson</h3>
                  <h4>Entrepreneur</h4>
                  <p>
                    <i className="bx bxs-quote-alt-left quote-icon-left"></i>
                    Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa
                    labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                    <i className="bx bxs-quote-alt-right quote-icon-right"></i>
                  </p>
                </div>
              </div>
              {/* End testimonial item */}
            </div>
            <div className="swiper-pagination"></div>
          </div>
        </div>
      </div>
      {/* End Testimonials Section */}

      {/* ======= Blog Section ======= */}
      <div id="blog" className="blog-area">
        <div className="blog-inner area-padding">
          <div className="blog-overly"></div>
          <div className="container ">
            <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12">
                <div className="section-headline text-center">
                  <h2>Latest News</h2>
                </div>
              </div>
            </div>
            <div className="row">
              {/* Start Left Blog */}
              <div className="col-md-4 col-sm-4 col-xs-12">
                <div className="single-blog">
                  <div className="single-blog-img">
                    <a href="blog.html">
                      <img src="assets/img/blog/1.jpg" alt="" />
                    </a>
                  </div>
                  <div className="blog-meta">
                    <span className="comments-type">
                      <i className="fa fa-comment-o"></i>
                      <a href="#">13 comments</a>
                    </span>
                    <span className="date-type">
                      <i className="fa fa-calendar"></i>2016-03-05 / 09:10:16
                    </span>
                  </div>
                  <div className="blog-text">
                    <h4>
                      <a href="blog.html">Assumenda repud eum veniam</a>
                    </h4>
                    <p>
                      Lorem ipsum dolor sit amet conse adipis elit Assumenda repud eum veniam optio modi sit explicabo nisi
                      magnam quibusdam.sit amet conse adipis elit Assumenda repud eum veniam optio modi sit explicabo nisi
                      magnam quibusdam.
                    </p>
                  </div>
                  <span>
                    <a href="blog.html" className="ready-btn">
                      Read more
                    </a>
                  </span>
                </div>
                {/* Start single blog */}
              </div>
              {/* End Left Blog*/}
              {/* Start Left Blog */}
              <div className="col-md-4 col-sm-4 col-xs-12">
                <div className="single-blog">
                  <div className="single-blog-img">
                    <a href="blog.html">
                      <img src="assets/img/blog/2.jpg" alt="" />
                    </a>
                  </div>
                  <div className="blog-meta">
                    <span className="comments-type">
                      <i className="fa fa-comment-o"></i>
                      <a href="#">130 comments</a>
                    </span>
                    <span className="date-type">
                      <i className="fa fa-calendar"></i>2016-03-05 / 09:10:16
                    </span>
                  </div>
                  <div className="blog-text">
                    <h4>
                      <a href="blog.html">Explicabo magnam quibusdam.</a>
                    </h4>
                    <p>
                      Lorem ipsum dolor sit amet conse adipis elit Assumenda repud eum veniam optio modi sit explicabo nisi
                      magnam quibusdam.sit amet conse adipis elit Assumenda repud eum veniam optio modi sit explicabo nisi
                      magnam quibusdam.
                    </p>
                  </div>
                  <span>
                    <a href="blog.html" className="ready-btn">
                      Read more
                    </a>
                  </span>
                </div>
                {/* Start single blog */}
              </div>
              {/* End Left Blog*/}
              {/* Start Right Blog*/}
              <div className="col-md-4 col-sm-4 col-xs-12">
                <div className="single-blog">
                  <div className="single-blog-img">
                    <a href="blog.html">
                      <img src="assets/img/blog/3.jpg" alt="" />
                    </a>
                  </div>
                  <div className="blog-meta">
                    <span className="comments-type">
                      <i className="fa fa-comment-o"></i>
                      <a href="#">10 comments</a>
                    </span>
                    <span className="date-type">
                      <i className="fa fa-calendar"></i>2016-03-05 / 09:10:16
                    </span>
                  </div>
                  <div className="blog-text">
                    <h4>
                      <a href="blog.html">Lorem ipsum dolor sit amet</a>
                    </h4>
                    <p>
                      Lorem ipsum dolor sit amet conse adipis elit Assumenda repud eum veniam optio modi sit explicabo nisi
                      magnam quibusdam.sit amet conse adipis elit Assumenda repud eum veniam optio modi sit explicabo nisi
                      magnam quibusdam.
                    </p>
                  </div>
                  <span>
                    <a href="blog.html" className="ready-btn">
                      Read more
                    </a>
                  </span>
                </div>
              </div>
              {/* End Right Blog*/}
            </div>
          </div>
        </div>
      </div>
      {/* End Blog Section */}

      {/* ======= Suscribe Section ======= */}
      <div className="suscribe-area">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs=12">
              <div className="suscribe-text text-center">
                <h3>Welcome to our eBusiness company</h3>
                <a className="sus-btn" href="#">
                  Get A quate
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* End Suscribe Section */}

      {/* ======= Contact Section ======= */}
      <div id="contact" className="contact-area">
        <div className="contact-inner area-padding">
          <div className="contact-overly"></div>
          <div className="container ">
            <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12">
                <div className="section-headline text-center">
                  <h2>Contact us</h2>
                </div>
              </div>
            </div>
            <div className="row">
              {/* Start contact icon column */}
              <div className="col-md-4">
                <div className="contact-icon text-center">
                  <div className="single-icon">
                    <i className="bi bi-phone"></i>
                    <p>
                      Call: +1 5589 55488 55
                      <br />
                      <span>Monday-Friday (9am-5pm)</span>
                    </p>
                  </div>
                </div>
              </div>
              {/* Start contact icon column */}
              <div className="col-md-4">
                <div className="contact-icon text-center">
                  <div className="single-icon">
                    <i className="bi bi-envelope"></i>
                    <p>
                      Email: info@example.com
                      <br />
                      <span>Web: www.example.com</span>
                    </p>
                  </div>
                </div>
              </div>
              {/* Start contact icon column */}
              <div className="col-md-4">
                <div className="contact-icon text-center">
                  <div className="single-icon">
                    <i className="bi bi-geo-alt"></i>
                    <p>
                      Location: A108 Adam Street
                      <br />
                      <span>NY 535022, USA</span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              {/* Start Google Map */}
              <div className="col-md-6">
                {/* Start Map */}
                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22864.11283411948!2d-73.96468908098944!3d40.630720240038435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sbg!4v1540447494452"
                  width="100%"
                  height={380}
                  frameBorder={0}
                  style={{ border: 0 }}
                  allowFullScreen
                ></iframe>
                {/* End Map */}
              </div>
              {/* End Google Map */}

              {/* Start  contact */}
              <div className="col-md-6">
                <div className="form contact-form">
                  <form action="forms/contact.php" method="post" className="php-email-form">
                    <div className="form-group">
                      <input type="text" name="name" className="form-control" id="name" placeholder="Your Name" required />
                    </div>
                    <div className="form-group mt-3">
                      <input
                        type="email"
                        className="form-control"
                        name="email"
                        id="email"
                        placeholder="Your Email"
                        required
                      />
                    </div>
                    <div className="form-group mt-3">
                      <input
                        type="text"
                        className="form-control"
                        name="subject"
                        id="subject"
                        placeholder="Subject"
                        required
                      />
                    </div>
                    <div className="form-group mt-3">
                      <textarea className="form-control" name="message" rows={5} placeholder="Message" required></textarea>
                    </div>
                    <div className="my-3">
                      <div className="loading">Loading</div>
                      <div className="error-message"></div>
                      <div className="sent-message">Your message has been sent. Thank you!</div>
                    </div>
                    <div className="text-center">
                      <button type="submit">Send Message</button>
                    </div>
                  </form>
                </div>
              </div>
              {/* End Left contact */}
            </div>
          </div>
        </div>
      </div>
    </main>
  </>
);

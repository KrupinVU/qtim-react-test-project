/* eslint-disable jsx-a11y/anchor-is-valid */
import { usePosts } from "hooks/usePosts.hook";
import { useParams } from "react-router-dom";
import React, { FC, useContext } from "react";
import { getBlogDate } from "utils/date.utils";

export const BlogDetails: FC = () => {
  const { PostsContext } = usePosts();
  const { getPost } = useContext(PostsContext);
  const { id } = useParams() as { id: string };
  const post = getPost(Number(id));
  if (!post) {
    return <></>;
  }
  const { createdAt, description, preview, image, title } = post;
  return (
    <main id="main">
      <div className="header-bg page-area">
        <div className="container position-relative">
          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12">
              <div className="slider-content text-center">
                <div className="header-bottom">
                  <div className="layer2">
                    <h1 className="title2">Blog Details </h1>
                  </div>
                  <div className="layer3">
                    <h2 className="title3">profesional Blog Page</h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="blog-page area-padding">
        <div className="container">
          <div className="row">
            <div className="col-lg-4 col-md-4">
              <div className="page-head-blog">
                <div className="single-blog-page">
                  <form action="#">
                    <div className="search-option">
                      <input type="text" placeholder="Search..." />
                      <button className="button" type="submit">
                        <i className="bi bi-search"></i>
                      </button>
                    </div>
                  </form>
                </div>
                <div className="single-blog-page">
                  <div className="left-blog">
                    <h4>recent post</h4>
                    <div className="recent-post">
                      <div className="recent-single-post">
                        <div className="post-img">
                          <a href="#">
                            <img src="assets/img/blog/1.jpg" alt="" />
                          </a>
                        </div>
                        <div className="pst-content">
                          <p>
                            <a href="#"> Redug Lerse dolor sit amet consect adipis elit.</a>
                          </p>
                        </div>
                      </div>
                      <div className="recent-single-post">
                        <div className="post-img">
                          <a href="#">
                            <img src="assets/img/blog/2.jpg" alt="" />
                          </a>
                        </div>
                        <div className="pst-content">
                          <p>
                            <a href="#"> Redug Lerse dolor sit amet consect adipis elit.</a>
                          </p>
                        </div>
                      </div>
                      <div className="recent-single-post">
                        <div className="post-img">
                          <a href="#">
                            <img src="assets/img/blog/3.jpg" alt="" />
                          </a>
                        </div>
                        <div className="pst-content">
                          <p>
                            <a href="#"> Redug Lerse dolor sit amet consect adipis elit.</a>
                          </p>
                        </div>
                      </div>
                      <div className="recent-single-post">
                        <div className="post-img">
                          <a href="#">
                            <img src="assets/img/blog/4.jpg" alt="" />
                          </a>
                        </div>
                        <div className="pst-content">
                          <p>
                            <a href="#"> Redug Lerse dolor sit amet consect adipis elit.</a>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="single-blog-page">
                  <div className="left-blog">
                    <h4>categories</h4>
                    <ul>
                      <li>
                        <a href="#">Portfolio</a>
                      </li>
                      <li>
                        <a href="#">Project</a>
                      </li>
                      <li>
                        <a href="#">Design</a>
                      </li>
                      <li>
                        <a href="#">wordpress</a>
                      </li>
                      <li>
                        <a href="#">Joomla</a>
                      </li>
                      <li>
                        <a href="#">Html</a>
                      </li>
                      <li>
                        <a href="#">Website</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="single-blog-page">
                  <div className="left-blog">
                    <h4>archive</h4>
                    <ul>
                      <li>
                        <a href="#">07 July 2016</a>
                      </li>
                      <li>
                        <a href="#">29 June 2016</a>
                      </li>
                      <li>
                        <a href="#">13 May 2016</a>
                      </li>
                      <li>
                        <a href="#">20 March 2016</a>
                      </li>
                      <li>
                        <a href="#">09 Fabruary 2016</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="single-blog-page">
                  <div className="left-tags blog-tags">
                    <div className="popular-tag left-side-tags left-blog">
                      <h4>popular tags</h4>
                      <ul>
                        <li>
                          <a href="#">Portfolio</a>
                        </li>
                        <li>
                          <a href="#">Project</a>
                        </li>
                        <li>
                          <a href="#">Design</a>
                        </li>
                        <li>
                          <a href="#">wordpress</a>
                        </li>
                        <li>
                          <a href="#">Joomla</a>
                        </li>
                        <li>
                          <a href="#">Html</a>
                        </li>
                        <li>
                          <a href="#">Masonry</a>
                        </li>
                        <li>
                          <a href="#">Website</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-8 col-sm-8 col-xs-12">
              <div className="row">
                <div className="col-md-12 col-sm-12 col-xs-12">
                  <article className="blog-post-wrapper">
                    <div className="post-thumbnail">
                      <img src={image} alt={title} />
                    </div>
                    <div className="post-information">
                      <h2>{title}</h2>
                      <div className="entry-meta">
                        <span className="author-meta">
                          <i className="bi bi-person"></i> <a href="#">admin</a>
                        </span>
                        <span>
                          <i className="bi bi-clock"></i> {getBlogDate(createdAt, true)}
                        </span>
                        <span className="tag-meta">
                          <i className="bi bi-folder"></i>
                          <a href="#">painting</a>,<a href="#">work</a>
                        </span>
                        <span>
                          <i className="bi bi-tags"></i>
                          <a href="#">tools</a>,<a href="#"> Humer</a>,<a href="#">House</a>
                        </span>
                        <span>
                          <i className="bi bi-chat"></i> <a href="#">6 comments</a>
                        </span>
                      </div>
                      <div className="entry-content">
                        <p>{description}</p>
                        <blockquote>
                          <p>{preview}</p>
                        </blockquote>
                        <p>{description}</p>
                      </div>
                    </div>
                  </article>
                  <div className="clear"></div>
                  <div className="single-post-comments">
                    <div className="comments-area">
                      <div className="comments-heading">
                        <h3>6 comments</h3>
                      </div>
                      <div className="comments-list">
                        <ul>
                          <li className="threaded-comments">
                            <div className="comments-details">
                              <div className="comments-list-img">
                                <img src="assets/img/blog/b02.jpg" alt="post-author" />
                              </div>
                              <div className="comments-content-wrap">
                                <span>
                                  <b>
                                    <a href="#">demo</a>
                                  </b>
                                  Post author
                                  <span className="post-time">October 6, 2014 at 4:25 pm</span>
                                  <a href="#">Reply</a>
                                </span>
                                <p>Quisque semper nunc vitae erat pellentesque, ac placerat arcu consectetur</p>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div className="comments-details">
                              <div className="comments-list-img">
                                <img src="assets/img/blog/b02.jpg" alt="post-author" />
                              </div>
                              <div className="comments-content-wrap">
                                <span>
                                  <b>
                                    <a href="#">admin</a>
                                  </b>
                                  Post author
                                  <span className="post-time">October 6, 2014 at 6:18 pm </span>
                                  <a href="#">Reply</a>
                                </span>
                                <p>
                                  Quisque orci nibh, porta vitae sagittis sit amet, vehicula vel mauris. Aenean at justo
                                  dolor. Fusce ac sapien bibendum, scelerisque libero nec
                                </p>
                              </div>
                            </div>
                          </li>
                          <li className="threaded-comments">
                            <div className="comments-details">
                              <div className="comments-list-img">
                                <img src="assets/img/blog/b02.jpg" alt="post-author" />
                              </div>
                              <div className="comments-content-wrap">
                                <span>
                                  <b>
                                    <a href="#">demo</a>
                                  </b>
                                  Post author
                                  <span className="post-time">October 6, 2014 at 7:25 pm</span>
                                  <a href="#">Reply</a>
                                </span>
                                <p>Quisque semper nunc vitae erat pellentesque, ac placerat arcu consectetur</p>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="comment-respond">
                      <h3 className="comment-reply-title">Leave a Reply </h3>
                      <span className="email-notes">
                        Your email address will not be published. Required fields are marked *
                      </span>
                      <form action="#">
                        <div className="row">
                          <div className="col-lg-4 col-md-4">
                            <p>Name *</p>
                            <input type="text" />
                          </div>
                          <div className="col-lg-4 col-md-4">
                            <p>Email *</p>
                            <input type="email" />
                          </div>
                          <div className="col-lg-4 col-md-4">
                            <p>Website</p>
                            <input type="text" />
                          </div>
                          <div className="col-lg-12 col-md-12 col-sm-12 comment-form-comment">
                            <p>Website</p>
                            <textarea id="message-box" cols={30} rows={10}></textarea>
                            <input type="submit" value="Post Comment" />
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

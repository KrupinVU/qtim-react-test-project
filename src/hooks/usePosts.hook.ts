import React, { createContext, useEffect, useState } from "react";
import { PostDto } from "models/post.dto";
import { getPosts } from "services/posts.service";

interface StateFields {
  posts: PostDto[];
  offset: number;
  count: number;
  currentPage: number;
}
const PostsContext = createContext({
  appState: {} as StateFields,
  pageContent: [] as PostDto[],
  setAppState: (state: StateFields) => {},
  goToPage: (page: number) => {},
  getPost: (id: number): PostDto | undefined => undefined,
});

export const usePosts = () => {
  const PAGINATION_STEP = 5;
  const initialState = {
    posts: [],
    offset: 0,
    count: 0,
    currentPage: 1,
  };

  const [appState, setAppState] = useState<StateFields>(initialState);

  const { posts, offset, currentPage } = appState;

  useEffect(() => {
    const fetchPosts = async () => {
      try {
        const result: PostDto[] = await getPosts();
        setAppState({
          offset,
          currentPage,
          posts: result || [],
          count: result?.length || 0,
        });
      } catch (e) {
        console.log(e);
      }
    };
    fetchPosts();
  }, [currentPage, offset]);

  const goToPage = (page: number) => {
    setAppState({
      ...appState,
      offset: (page - 1) * PAGINATION_STEP,
      currentPage: page,
    });
  };
  const getPost = (id: number) => posts.find((post) => Number(post.id) === id);

  const pageContent = posts.slice(offset, offset + PAGINATION_STEP);

  const providerValue = { appState, pageContent, setAppState, goToPage, getPost };

  return { PostsContext, providerValue };
};

const MONTHS = [
  "january",
  "february",
  "march",
  "april",
  "may",
  "june",
  "july",
  "august",
  "september",
  "october",
  "novermber",
  "december",
];

export const getBlogDate = (date: string, shorten?: boolean) => {
  const fullDate = new Date(date);
  const day = fullDate.getDate();
  const month = fullDate.getMonth();
  const year = fullDate.getFullYear();
  const hours = fullDate.getHours();
  const minutes = fullDate.getMinutes();
  const seconds = fullDate.getSeconds();

  if (shorten) {
    return `${MONTHS[month - 1]} ${day}, ${year}`;
  }

  return `${year}-${month}-${day} / ${hours}:${minutes}:${seconds}`;
};

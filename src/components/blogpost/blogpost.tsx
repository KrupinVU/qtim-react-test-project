/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { FC } from "react";
import { getBlogDate } from "utils/date.utils";

export const BlogPost: FC<{ blog: any }> = ({ blog }) => {
  const { createdAt, description, id, image, title } = blog;
  return (
    <div className="col-md-12 col-sm-12 col-xs-12">
      <div className="single-blog">
        <div className="single-blog-img">
          <a href={`/blog/${id}`}>
            <img src={image} alt={title} />
          </a>
        </div>
        <div className="blog-meta">
          <span className="comments-type">
            <i className="bi bi-chat"></i>
            <a href="#">10 comments</a>
          </span>
          <span className="date-type">
            <i className="bi bi-calendar"></i>
            {getBlogDate(createdAt)}
          </span>
        </div>
        <div className="blog-text">
          <h4>
            <a href="#">{title}</a>
          </h4>
          <p>{description}</p>
        </div>
        <span>
          <a href={`/blog/${id}`} className="ready-btn">
            Read more
          </a>
        </span>
      </div>
    </div>
  );
};

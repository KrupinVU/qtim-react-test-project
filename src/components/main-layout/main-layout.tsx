import React, { FC, ReactNode } from "react";
import { Footer, Header } from "..";

export const MainLayout: FC<{ children: ReactNode }> = ({ children }) => {
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  );
};

/* eslint-disable jsx-a11y/anchor-is-valid */
import { usePosts } from "hooks/usePosts.hook";
import React, { FC, useContext } from "react";

export const Pagination: FC = () => {
  const { PostsContext } = usePosts();
  const {
    appState: { count, currentPage },
    goToPage,
  } = useContext(PostsContext);
  const pagesCount = Math.ceil(count / 5) || 1;
  const buttons = Array(pagesCount)
    .fill(0)
    .map((_, index) => (
      <li key={index} className={`page-item ${currentPage === index + 1 ? "active" : ""}`}>
        <a href="#" className="page-link" onClick={() => goToPage(index + 1)}>
          {index + 1}
        </a>
      </li>
    ));
  return (
    <div className="col-md-8 col-sm-8 col-xs-12">
      <div className="row">
        <div className="blog-pagination">
          <ul className="pagination">
            {currentPage > 1 && (
              <li className="page-item">
                <a href="#" className="page-link" onClick={() => goToPage(currentPage - 1)}>
                  &lt;
                </a>
              </li>
            )}
            {buttons}
            {currentPage < pagesCount && (
              <li className="page-item">
                <a href="#" className="page-link" onClick={() => goToPage(currentPage + 1)}>
                  &gt;
                </a>
              </li>
            )}
          </ul>
        </div>
      </div>
    </div>
  );
};

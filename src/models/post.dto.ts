export interface PostDto {
  createdAt: string;
  description: string;
  preview: string;
  id: string;
  image: string;
  title: string;
}

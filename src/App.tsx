import React, { FC } from "react";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import { MainLayout } from "components";

import { Home } from "views/home/home";
import { Blog } from "views/blog";
import { BlogDetails } from "views/details";

import { usePosts } from "hooks/usePosts.hook";

const App: FC = () => {
  const { PostsContext, providerValue } = usePosts();
  return (
    <MainLayout>
      <Router>
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <PostsContext.Provider value={providerValue}>
            <Route path="/blog" exact>
              <Blog />
            </Route>
            <Route path="/blog/:id">
              <BlogDetails />
            </Route>
          </PostsContext.Provider>
        </Switch>
      </Router>
    </MainLayout>
  );
};

export default App;
